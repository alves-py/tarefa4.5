using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using NAudio.Wave;

namespace Tarefa4._0.Views;

public partial class MainWindow : Window
{
    private HashSet<Key> PressedKeys { get;} = new();
    private DispatcherTimer? timer;
    private WaveOutEvent waveOut;
    private AudioFileReader? audioFileReader;
    
    public MainWindow()
    {
        InitializeComponent();
        Personagem = this.FindControl<Image>("Personagem");
        
        waveOut = new WaveOutEvent();
        
        KeyDown += HandleKeyDown;
        KeyUp += HandleKeyUp;
    }
    
    private void HandleKeyDown(object? sender, KeyEventArgs e)
    {
        PressedKeys.Add(e.Key);
        timer?.Stop();
        MovePlayer(sender, e);
    }

    private void HandleKeyUp(object? sender, KeyEventArgs e)
    {
        PressedKeys.Remove(e.Key);
        timer?.Stop();
    }

    public void MovePlayer(object sender, KeyEventArgs  e)
    {
        switch (e.Key)
        {
            case Key.Right:
                runningAction();
                break;
            
            case Key.Up:
                JumpAction();
                break;

            case Key.Down:
                DownAction();
                break;
        }
        
    }

    public async void runningAction()
    {
        int currentSpriteIndex = 2;
        
        LoadAudio();
        waveOut.Play();
        while (currentSpriteIndex <= 6)
        {
            var index = currentSpriteIndex;
            Personagem.Source = new Bitmap($"C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/{index}.png");
            
            await Task.Delay(160);

            currentSpriteIndex++;
        }
        
        waveOut.Stop();
        Personagem.Source = new Bitmap("C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/1.png");
    }

        public async void JumpAction()
    {

        int jumpIndex = 1;

        LoadAudio();
        waveOut.Play();

        while (jumpIndex <= 3)
        {
            Personagem.Source = new Bitmap($"C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/jump{jumpIndex}.png");

            await Task.Delay(200);

            jumpIndex++;
        }

        waveOut.Stop();
        Personagem.Source = new Bitmap("C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/1.png");
    }

    public async void DownAction(){
        Personagem.Source = new Bitmap($"C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/down.png");

        await Task.Delay(400);

        Personagem.Source = new Bitmap("C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/Sprites/Personagem/1.png");
    }
    
    public void LoadAudio()
    {
        string audioFilePath = "C:/Users/Felipe Alves/Documents/Jala University/2024/Programming 3/semana4/Tarefa4.0/Tarefa4.0/Assets/SoundEffects/running-sounds-6003.mp3";

        if (waveOut.PlaybackState == PlaybackState.Playing)
        {
            waveOut.Stop();
        }

        audioFileReader = new AudioFileReader(audioFilePath);
        waveOut.Init(audioFileReader);
    }

    
}